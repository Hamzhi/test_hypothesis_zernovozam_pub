import React from 'react';
import {Provider} from "react-native-paper";
import {theme} from "./src/core/theme";
import {NavigationContainer} from "@react-navigation/native";
import LocationScreen from "./src/screens/AuthScreens/LocationScreen";
import {AuthProvider} from "./src/services/AuthProvider";
import {createNativeStackNavigator} from "@react-navigation/native-stack";
import MapScreen from "./src/screens/MainScreens/MapScreen";
import StartScreen from "./src/screens/StartScreen";
import PhoneScreen from "./src/screens/AuthScreens/PhoneScreen";
import AuthenticationScreen from "./src/screens/AuthScreens/AuthenticationScreen";


const Stack = createNativeStackNavigator()
export default function App() {
  return (
      <AuthProvider>
      <Provider theme={theme}>
          <NavigationContainer>
            <Stack.Navigator
                initialRouteName="Start"
                screenOptions={{headerShown: false}}
            >
                <Stack.Screen name="Start" component={StartScreen}/>
                <Stack.Screen name="Phone" component={PhoneScreen}/>
                <Stack.Screen name="Authentication" component={AuthenticationScreen}/>
                <Stack.Screen name="Location" component={LocationScreen}/>
                <Stack.Screen name="Map" component={MapScreen}/>
            </Stack.Navigator>
          </NavigationContainer>
      </Provider>
      </AuthProvider>
  );
}
