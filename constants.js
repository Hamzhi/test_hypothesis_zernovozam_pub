export const API_URL = 'http://tapi2.zernovozam.ru/new-zernovozam/';

export const STORAGE_KEY = {
  token: 'zernoUserToken',
  stevedores: 'zernoStevedoreIds',
  mapLocation: 'zernoMapLocation',
  userLocation: 'zernoUserLocation',
  radius: 'zernoUserRadius',
  isSearch: 'zernoIsSearch',
  showed: 'zernoShowed'
};

export const LOCATION_STATUSES = {
  untouched: 'untouched', // не запрашивали
  granted: 'granted', // предоставлен
  denied: 'denied', // не предоставлен
};
