import * as React from 'react';
import { ScrollView, View } from 'react-native';
import { Dialog, Portal } from 'react-native-paper';
import Button from './Button';
import CustomWebView from '../screens/MainScreens/CustomWebView';

const OnBoardingDialog = (props) => {
  const { data, loading, onSuccess } = props

  return (
    <Portal>
      <Dialog visible={Boolean(data)} dismissable={false} style={{
        height: '90%',
        boxShadow: '0px 5px 10px rgba(34, 43, 69, 0.15)',
        borderRadius: 12
      }}>
        {data?.text && (
          <Dialog.Title style={{ textAlign: 'center' }}>
            {data.text}
          </Dialog.Title>
        )}
        <Dialog.Content style={{ paddingHorizontal: 20, flex: 1 }}>
          <CustomWebView uri={data?.uri} />
        </Dialog.Content>
        <Dialog.Actions style={{ justifyContent: 'center' }}>
          <View style={{ width: 230 }}>
            <Button loading={loading} disabled={loading} mode="contained" onPress={onSuccess}>
              {data?.buttonText || 'ХОРОШО'}
            </Button>
          </View>
        </Dialog.Actions>
      </Dialog>
    </Portal>
  );
};

export default OnBoardingDialog;
