import React from 'react'
import { StyleSheet, View } from 'react-native'
import {Card, Title} from "react-native-paper";
import Paragraph from "./Paragraph";
import {useKeyboard} from "../hooks/useKeyboard";
import ButtonIcon from "./ButtonIcon";



const MapFilterCard = ({ text, showFilter= false, showFilterPress,  children }) => {
    const isKeyBoardOpen = useKeyboard();

    return (
      <View style={[styles.container,
          {
              bottom: isKeyBoardOpen ? null : '12%',
              top: isKeyBoardOpen? '50%' : null,
          }
      ]}>
          {showFilter && (
            <View style={styles.filterContainer}>
                <Title style={styles.filterTitle}>изменить фильтр</Title>
                <ButtonIcon onPress={ showFilterPress} />
            </View>)
          }
          <Card>
              <Card.Content>
                  {text && (
                    <Paragraph style={{ color: '#000', marginBottom: 16 }}>
                      {text}
                    </Paragraph>
                  )}
                  <View>
                    {children}
                  </View>
              </Card.Content>
          </Card>
      </View>
    )

}

const styles = StyleSheet.create({
    container : {
        position: 'absolute',
        zIndex: 999999999,
        width: '90%'
    },
    filterContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        marginBottom: 20,
        alignItems: 'center'
    },
    filterTitle: {
        marginRight: 16,
        textTransform: 'uppercase',
        color: '#000',
        fontWeight: 'bold'
    }
})

export default MapFilterCard
