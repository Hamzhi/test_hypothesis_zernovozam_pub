import React from 'react'
import {View, StyleSheet, KeyboardAvoidingView, TouchableWithoutFeedback, Keyboard, Platform} from 'react-native'
import { theme } from '../core/theme'
import SwipeButton from './SwipeButton';

export default function Background({ children }) {
  return (
    <View style={styles.background}>
      <KeyboardAvoidingView
          style={styles.container}
          behavior={Platform.OS === "ios" ? "padding" : "height"}
          keyboardVerticalOffset={Platform.OS === "ios" ? 0 : -10}>
        {children}
      </KeyboardAvoidingView>
    </View>
  )
}

const styles = StyleSheet.create({
  background: {
    flex: 1,
    width: '100%',
    backgroundColor: theme.colors.tint,
  },
  container: {
    flex: 1,
    // padding: 20,
    width: '100%',
    maxWidth: '95%',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
})
