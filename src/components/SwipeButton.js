import React, {useEffect, useMemo, useRef, useState} from 'react'
import {View, Text, StyleSheet, Dimensions, Image} from 'react-native'
import Animated from "react-native-reanimated";
import {PanGestureHandler, State} from "react-native-gesture-handler";
import { getStatusBarHeight } from 'react-native-status-bar-height';
const {width} = Dimensions.get('window')

const rightIcon = require('../assets/right.png')
const closeIcon = require('../assets/close.png')

const {
    View: Aview,
    Value,
    event,
    set,
    block,
    cond,
    lessThan,
    greaterThan,
    add,
    eq,
    useCode,
    call,
} = Animated;

const MAX_WIDTH = width - width * 0.1

const SwipeButton = ({
    status,
    setStatus,
    disabled = false,
    textOn = 'Отменить поиск',
    textOff = 'Получать работу',
    colorOn = '#E4E9F2',
    colorOff = '#41D5FB',
}) => {
    const transX = useRef(new Value(0)).current
    const offsetX = useRef(new Value(0)).current

    useCode(() => [
        call([transX], ([value]) => {
            if (value === MAX_WIDTH){
                setStatus(true)
            } else if (value === 0){
                setStatus(false)
            }
        })
    ],[transX])

    const onGestureHandle = useMemo(() => {
        return event([
            {
                nativeEvent: ({translationX: x, state}) =>
                    block([
                        cond(lessThan(add(offsetX, x), 0), set(transX, 0),[
                            cond(
                                greaterThan(add(offsetX, x), MAX_WIDTH),
                                set(transX, MAX_WIDTH),
                                set(transX, add(offsetX, x)),
                            ),
                        ]),
                        cond(eq(state, State.END),[
                            cond(
                                lessThan(transX, MAX_WIDTH / 2),
                                block([set(transX, 0), set(offsetX, 0)]),
                                block([set(transX, MAX_WIDTH), set(offsetX, MAX_WIDTH)])
                            ),
                        ]),
                    ]),
            },
        ])

    }, [transX, offsetX])

    return (
        <View style={[styles.container, { backgroundColor: !status ? colorOn : colorOff}]}>
            <View>
                <Text>{status ? textOn : textOff}</Text>
            </View>
            <PanGestureHandler
              enabled={!disabled}
              onGestureEvent={onGestureHandle}
              onHandlerStateChange={onGestureHandle}
            >
                <Aview style={[styles.arrow, {
                    transform: [{translateX: transX}],
                    left: !status ? 4 : -50
                }]}>
                    <Image
                      style={[{width: 22, height: 22}]}
                      source={!status ? rightIcon : closeIcon}
                    />
                </Aview>

            </PanGestureHandler>
        </View>
    )
}
const styles = StyleSheet.create({
    container : {
        position: 'absolute',
        zIndex: 999999999,
        width: MAX_WIDTH,
        top: 10 + getStatusBarHeight(),
        height: 50,
        borderRadius: 50,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },
    arrow : {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        height: 46,
        width: 46,
        borderRadius: 22,
        backgroundColor: '#fff',
        position: 'absolute',
    }
})


export default SwipeButton
