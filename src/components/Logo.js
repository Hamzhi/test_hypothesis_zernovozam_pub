import React from 'react'
import { Image, StyleSheet } from 'react-native'

export default function Logo() {
  return <Image style={styles.image} source={require("./../assets/main-logo-png.png")} />
}

const styles = StyleSheet.create({
  image: {
    width: 118,
    height: 175,
    marginBottom: 8,

  },

})
