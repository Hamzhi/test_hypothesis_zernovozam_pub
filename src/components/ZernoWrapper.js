import React from 'react'
import { StyleSheet, ImageBackground, StatusBar, View } from 'react-native'

const image = require('../assets/main-bg.jpg')

const ZernoWrapper = ({children, webview = false}) => (
  <ImageBackground source={image} style={[styles.container, {flex: webview ? 0 : 1 }]}>
    <View colors={['#214A67', 'rgba(33, 74, 103, 0.5)']} style={styles.bg}>
      <StatusBar barStyle="light-content"/>
      {children}
    </View>
  </ImageBackground>
)

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  bg: {
    width: '100%',
    height: '100%',
    justifyContent: 'center'
  }
})

export default ZernoWrapper
