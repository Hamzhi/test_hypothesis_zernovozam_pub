import React, { useState } from "react";
import {StyleSheet, Text, View, Button, Alert, TextInput} from 'react-native';

import OTPTextView from 'react-native-otp-textinput';
import {theme} from "../core/theme";


const OtpSmsCode = ({ route, navigation, setSmsCode, invalidCode= false, setInvalidCode }) => {
  return (
      <View >
        <OTPTextView
            inputCount={4}
            handleTextChange={(e) => {
              setInvalidCode(false)
              setSmsCode(e)
            }}
            containerStyle={styles.textInputContainer}
            textInputStyle={[styles.roundedTextInput, {borderRadius: 25, width: 50, height: 50}]}
            tintColor="#41D5FB"
            offTintColor="#E4E9F2"

        />
        <Text
            style={[
              styles.error,
              !invalidCode && { color: 'white' },
            ]} >Вы ввели неверный код</Text>
      </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    justifyContent: "space-between",
    alignItems: "center",
  },
  roundedTextInput: {
    borderRadius: 100,
    borderWidth: 2,
    borderColor: '#E4E9F2',
    color: '#222B45',
    fontSize: 32,
    fontWeight: "bold"
  },

  underlineStyleBase: {
    justifyContent: "space-between",


    width: 60,
    height: 60,
    borderWidth: 1,
    borderColor: '#E4E9F2',
    fontWeight: "bold",
    lineHeight: 40,
    color: "#222B45",
    fontSize: 34,
    borderRadius: 25,
    shadowColor: 'red'
  },

  underlineStyleHighLighted: {
    borderColor: "#03DAC6",
  },

  prompt: {
    fontSize: 24,
    paddingHorizontal: 30,
    paddingBottom: 20,
  },

  message: {
    fontSize: 16,
    paddingHorizontal: 30,
  },

  textInputContainer: {
    marginBottom: 20,
  },

  error: {
    color: "red",
  },
});

export default OtpSmsCode;
