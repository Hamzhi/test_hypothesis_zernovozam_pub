import React from 'react'
import {Image, StyleSheet} from 'react-native'
import {TouchableOpacity} from "react-native-gesture-handler";

export default function ButtonIcon({ onPress }) {
  return (
      <TouchableOpacity activeOpacity={.5} onPress={onPress} style={styles.img}>
        <Image
            style={styles.img}
            source={require('../assets/buttonFilter.png')}
        />
      </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  img: {
      height: 48,
      width: 48
  },
})
