import React, {useRef, useState} from 'react'
import { View, StyleSheet, Text } from 'react-native'
import { TextInput as Input } from 'react-native-paper'
import { theme } from '../core/theme'
import { MaskedTextInput} from "react-native-mask-text";
import PhoneInput from "react-native-phone-number-input";

export default function PhoneInputMask({ errorText, description, ...props }) {
    const [value, setValue] = useState("");
    const [valid, setValid] = useState(false);
    const [showMessage, setShowMessage] = useState(false);
    const phoneInput = useRef(null);


    return (
        <View style={styles.container}>
            <PhoneInput
                ref={phoneInput}
                defaultValue={value}
                defaultCode="RU"
                onChangeFormattedText={(text) => {
                    setValue(text);
                }}
                withDarkTheme
                withShadow={false}
                autoFocus
                disableArrowIcon={false}
            />
        </View>)
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        marginVertical: 12,
    },
    input: {
        backgroundColor: theme.colors.surface,
    },
    description: {
        fontSize: 13,
        color: theme.colors.secondary,
        paddingTop: 8,
    },
    error: {
        fontSize: 13,
        color: theme.colors.error,
        paddingTop: 8,
    },
})
