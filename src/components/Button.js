import React from 'react'
import { StyleSheet } from 'react-native'
import { Button as PaperButton } from 'react-native-paper'
import { theme } from '../core/theme'

export default function Button({ mode, style, size, ...props }) {
  return (
    <PaperButton
      style={[
        styles.button,
        mode === 'outlined' && { backgroundColor: theme.colors.surface, color: '#000' },
          size === 'big' && { paddingVertical: 5 },
        style,
      ]}
      labelStyle={[styles.text, mode === 'outlined' && { color: '#000' },]}
      mode={mode}
      {...props}
    />
  )
}

const styles = StyleSheet.create({
  button: {
    width: '100%',
    marginVertical: 10,
    paddingVertical: 2,
    borderRadius: 15
  },
  text: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 15,
    lineHeight: 26,
    textTransform: 'none',
    textAlign: 'center'
  },
})
