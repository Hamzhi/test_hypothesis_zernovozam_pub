import React from 'react'
import { View, StyleSheet, Text } from 'react-native'
import { TextInput as Input } from 'react-native-paper'
import { theme } from '../core/theme'

export default function TextInput({ value, errorText, description,onChange, ...props }) {
  return (
    <View style={styles.container}>
      <Input
        value={String(value)}
        defaultValue={String(value)}
        keyboardType="number-pad"
        style={styles.inputText}
        selectionColor={theme.colors.primary}
        underlineColor={theme.colors.primary}
        outlineColor={theme.colors.borderInput}
        mode="outlined"
        onChangeText={text => onChange(Number(text))}
        right={<Input.Affix text="| КМ" />}
        {...props}
      />
      {description && !errorText ? (
        <Text style={styles.description}>{description}</Text>
      ) : null}
      {errorText ? <Text style={styles.error}>{errorText}</Text> : null}
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    marginVertical: 5,
    borderRadius: 15

  },
  inputText: {
    backgroundColor: theme.colors.white,
    borderRadius: 15
  },
  description: {
    fontSize: 13,
    color: theme.colors.secondary,
    paddingTop: 8,
  },
  error: {
    fontSize: 13,
    color: theme.colors.error,
    paddingTop: 8,
  },
})
