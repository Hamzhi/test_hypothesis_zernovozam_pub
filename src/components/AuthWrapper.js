import React from 'react'
import { StyleSheet, SafeAreaView, ImageBackground, Platform, StatusBar, Text, View } from 'react-native'
import {theme} from "../core/theme";

const image = require('../assets/main-bg.jpg')

const AuthWrapper = ({ children }) => (
  <ImageBackground source={image} style={styles.container}>
    <SafeAreaView style={styles.safe}>
      <StatusBar barStyle="dark-content" />
      <Text style={styles.title}>ООО “Порт транзит”</Text>
    </SafeAreaView>
    <View style={styles.children}>
      {children}
    </View>
  </ImageBackground>

)

const styles = StyleSheet.create({
  container: { flex: 1 },
  safe: {flex: 1, paddingTop: Platform.OS === 'android' ? StatusBar.currentHeight : 0},
  children: { flex: 1, justifyContent: 'flex-start' },
  title: {
    fontWeight: 'bold',
    fontSize: 16,
    lineHeight: 19,
    color: theme.colors.primary,
    paddingHorizontal: 13,
    paddingVertical: 9
  }
})

export default AuthWrapper
