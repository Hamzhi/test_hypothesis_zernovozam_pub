import React from 'react'
import { ActivityIndicator } from 'react-native'

const ZernoLoader = ({styles = {flex: 1}}) => (
  <ActivityIndicator size="large" color="#37BFCA" style={styles} />
)

export default ZernoLoader
