import React from 'react'
import {View, StyleSheet, Image} from 'react-native'


export default function MapStyledMarker() {
  return (
      <View>
          <Image
              style={styles.image}
              source={require('../assets/markerMap.png')}
          />
      </View>
  )
}

const styles = StyleSheet.create({
    image: {
        width: 53,
        height: 76
    },
})
