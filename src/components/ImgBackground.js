import React from 'react'
import { View, StyleSheet, KeyboardAvoidingView } from 'react-native'
import { theme } from '../core/theme'
import {ImageBackground} from "react-native";

export default function ImgBackground({ children }) {
  return (
      <ImageBackground
          style={styles.background}
          source={require("../assets/main-bg.jpg")}
      >
      <KeyboardAvoidingView style={styles.container} behavior="padding">

        {children}
      </KeyboardAvoidingView>
      </ImageBackground>
  )
}

const styles = StyleSheet.create({
  background: {
      flex: 1,
      justifyContent: "flex-end",
      alignItems: "center",
    // backgroundColor: theme.colors.tint,
  },

  container: {
    flex: 1,
    padding: 20,
    width: '100%',
    maxWidth: 340,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
})
