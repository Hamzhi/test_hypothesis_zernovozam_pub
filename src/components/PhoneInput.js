import React from 'react'
import { View, Text } from 'react-native'
import { TextInputMask } from 'react-native-masked-text'
import * as Animatable from 'react-native-animatable'
import { StyleSheet, Platform } from 'react-native'


const PhoneInput = ({ onChange, isValid, val, setOpened = () => {} }) => (
  <View>
    <Text style={styles.text_footer}>Ваш номер телефона:</Text>
    <View style={[styles.action, !isValid ? styles.err : {}]}>
      <TextInputMask
        keyboardType="number-pad"
        type="custom"
        value={val}
        options={{ mask: '+7 (999) 999-99-99' }}
        placeholder="+7"
        placeholderTextColor="#000"
        onChangeText={onChange}
        style={styles.textInput}
        // onFocus={setOpened}
        returnKeyType="done"
      />

      {/*{(isValid && !!val) && (*/}
      {/*  <Animatable.View animation="bounceIn">*/}
      {/*    <Feather name="check-circle" color="#00ff00" size={20} />*/}
      {/*  </Animatable.View>*/}
      {/*)}*/}
    </View>
    {!isValid && (
      <Animatable.View animation="fadeInLeft" duration={500}>
        <Text style={styles.errorMsg}>Введите номер телефона полностью</Text>
      </Animatable.View>
    )}
  </View>
)

export default PhoneInput


const styles =  StyleSheet.create({
    text_footer: {
        color: '#333',
        fontWeight: '400',
        fontSize: 14,
        lineHeight: 16,
        marginBottom: 15
    },
    action: {
        flexDirection: 'row',
        paddingBottom: 5,
        alignItems: 'center'
    },
    err: {
        borderBottomColor: '#f56c6c'
    },
    textInput: {
        height: 50,
        borderColor: "#C8D3E8",
        flexDirection: "row",
        paddingHorizontal: 12,
        borderRadius: 10,
        backgroundColor: 'white',
        alignItems: 'center',
        borderWidth: 1,
        flex: 1,
        marginTop: Platform.OS === 'ios' ? 0 : -12,
        fontSize: 18,
        fontWeight: "500",
        color: '#333',
    },
    errorMsg: {
        color: '#f56c6c',
        fontSize: 14,
    }
})
