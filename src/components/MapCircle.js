import React, { useRef } from 'react'

import MapView from "react-native-maps";

export default function MapCircle({ location, radius }) {
      let circleRef = useRef(null)
      const onLayout = () => {
            circleRef.current.setNativeProps({
                  strokeColor: 'transparent',
                  fillColor: 'rgba(65, 213, 251, 0.4)',
            })}
      return (
        <MapView.Circle
          ref={circleRef}
          onLayout={onLayout}
          center={location}
          radius={radius * 1000}
        />
      )
}
