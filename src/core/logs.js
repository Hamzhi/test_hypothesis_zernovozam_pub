export const logs = {
  getSms: {
    action: 'getSms',
    description: 'СМС подтверждение (ввел номер телефона, но не подтвердил его)'
  },
  sendSms: {
    action: 'getSms',
    description: 'Разрешение геолокации (подтвердил номер, но не разрешил/не запретил гео отслеживание)'
  },
  setGeoConfig: {
    action: 'getSms',
    description: 'Первый онбординг "как настроить фильтры" (разрешил/запретил отслеживать гео, но не пропустил первый онбординг)'
  },
  passFirstOnBoarding: {
    action: 'getSms',
    description: 'Первый фильтр точка+радиус (пропустил первый онбординг, но еще не установил точку и/или радиус)'
  },
  setFilters: {
    action: 'getSms',
    description: 'Установка портов (установил точку и радиус, но не выбрал порт(ы))'
  },
  setPort: {
    action: 'getSms',
    description: 'Второй онбординг "как включить шильдик и стать в активной поиске" (выбрал порты, но не пропустил второй онбординг)'
  },
  passSecondOnBoarding: {
    action: 'getSms',
    description: 'Установлены все фильтры, но не начал искать работу (пропустил второй онбординг, но не включил шильдик)',
  },
  findJobs: {
    action: 'getSms',
    description: 'Третий онбординг "как работать с предложениями" (включил шильдик, но не пропустил онбординг)'
  },
  passThirdOnBoarding: {
    action: 'getSms',
    description: 'Ищет работу (пропустил онбординг, не переключал шильдик обратно)'
  },
  calling: {
    action: 'getSms',
    description: 'Позвонил (позвонил)'
  }
}