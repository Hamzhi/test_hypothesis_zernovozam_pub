import { DefaultTheme } from 'react-native-paper'

export const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    text: '#222B45',
    primary: '#41D5FB',
    tint: '#FCFCFD',
    secondary: '#414757',
    border: '#C9DDEC',
    error: '#f13a59',
    success: '#00B386',
    google: '#2E7D32',
    white: '#fff',
    borderInput: '#E4E9F2'

  },
}
