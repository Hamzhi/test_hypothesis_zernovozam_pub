import axios from './axiosSetting'

const actionBid = 'bid-requests'
const actionBids = 'bids'

export default {
  createBid(data) {
    return axios.post(actionBid, data)
  }
}
