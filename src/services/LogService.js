import axios from './axiosSetting'

const action = 'frontend-logs/create'

export default {
  async sendLog(data) {
    return await axios.post(action, data)
  }
}
