import React from 'react'
import {writeToStorage} from './StorageService'
import { Dimensions } from 'react-native';
import { LOCATION_STATUSES } from '../../constants';
const { height, width } = Dimensions.get( 'window' );
const LATITUDE_DELTA = 5;
const LONGITUDE_DELTA = LATITUDE_DELTA * (width / height);

const INITIAL_LOCATION = {
  latitude: 45.04484 ,
  longitude:  38.97603,
  latitudeDelta: LATITUDE_DELTA,
  longitudeDelta: LONGITUDE_DELTA
}

const INITIAL_RADIUS = 50


export const AuthContext = React.createContext({
  userToken: '',
  phone: '',
  stevedoreIds: [1, 2, 3, 4],
  userLocation: { status: LOCATION_STATUSES.untouched },
  mapLocation: INITIAL_LOCATION,
  radiusMap: INITIAL_RADIUS,
  searchingJob: false,
  showed: 0,
  setShowed: () => {},
  setSearchingJob: () => {},
  login: () => {},
  setRadius: () => {},
  setPhone: () => {},
  setMapLocation: () => {},
  setUserLocation: () => {},
  setStevedore: () => {}
})

export const AuthProvider = ({children}) => {
  const [stevedoreIds, setStevedoreIds] = React.useState([1, 2, 3, 4]);
  const [userToken, setUserToken] = React.useState('')
  const [phone, setPhone] = React.useState('')
  const [searchingJob, setSearchingJob] = React.useState(false)
  const [showed, setShowed] = React.useState(0)
  const [radiusMap, setRadiusMap] = React.useState(INITIAL_RADIUS)
  const [mapLocation, setMapLocation] = React.useState(INITIAL_LOCATION)
  const [userLocation, setUserLocation] = React.useState({ status: LOCATION_STATUSES.untouched })
  return (
    <AuthContext.Provider
      value={{
        mapLocation,
        userLocation,
        userToken,
        phone,
        stevedoreIds,
        radiusMap,
        showed,
        searchingJob,
        setPhone: (phoneUser) => {
          setPhone(phoneUser)
        },
        setStevedore: (stevedore, save = true) => {
          const newArr = stevedoreIds.includes(stevedore)
            ? stevedoreIds.filter(item => item !== stevedore)
            : [...stevedoreIds, stevedore]
          setStevedoreIds(newArr)
          if (save) writeToStorage(newArr, 'stevedores')
        },
        setRadius: (radius, save = true) => {
            setRadiusMap(radius)
            if (save) writeToStorage(radius, 'radius')
        },
        setMapLocation: (location,  save = true) => {
          setMapLocation(location)
            if (save) writeToStorage(location, 'mapLocation')
        },
        setUserLocation: (location,  save = true) => {
          setUserLocation(location)
            if (save) writeToStorage(location, 'userLocation')
        },
        setSearchingJob: (isSearch,  save = true) => {
          setSearchingJob(isSearch)
            if (save) writeToStorage(isSearch, 'isSearch')
        },
        setShowed: (showed,  save = true) => {
          setShowed(showed)
            if (save) writeToStorage(showed, 'showed')
        },
        login: (token, save = true) => {
          setUserToken(token)
          if (save)
            writeToStorage(token, 'token')
        }
      }}
    >
      {children}
    </AuthContext.Provider>
  )
}
