import axios from './axiosSetting'

const actionRequestSms = "auth/request-sms"
const actionGetToken = "auth/get-token"

export default {
  authRequestSms(data) {
    return axios.post(actionRequestSms, data)
  },
  authGetToken(data) {
    return axios.post(actionGetToken, data)
  }
}
