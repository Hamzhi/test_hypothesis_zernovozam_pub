import axios from 'axios'
import { readFromStorage, removeFromStorage } from './StorageService'
import {API_URL} from "../../constants";

const ax = axios.create({
  baseURL: API_URL,
  timeout: 30000
})

ax.interceptors.request.use(
  async config => {
    const newConfig = config
    const token = await readFromStorage('token')
    if (token) newConfig.headers.Authorization = `Bearer ${token}`
    return newConfig
  },
  err => Promise.reject(err)
)

ax.interceptors.response.use(
  res => res,
  async error => {
    if (401 === error?.response?.status) {
      await removeFromStorage('token')
    }
    if (422 === error?.response?.status) {
      return Promise.reject(error)
    }
    console.log('error', error)
    return Promise.reject(error?.response || error)
  }
)

export default ax
