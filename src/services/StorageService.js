import {STORAGE_KEY} from "../../constants";
import AsyncStorage from '@react-native-async-storage/async-storage'

const readFromStorage = async (key) => {
  try {
    // await clearStorage()
    const res = await AsyncStorage.getItem(STORAGE_KEY[key])
    return JSON.parse(res)
  } catch (error) {
    console.log(error);
  }
}

const writeToStorage = async (newValue, key) => {
  try {
    const val = newValue !== null ? JSON.stringify(newValue) : newValue
    await  AsyncStorage.setItem(STORAGE_KEY[key], val)
  } catch (error) {
    console.log(error);
  }
}


const removeFromStorage = async (key) => {
  try {
    return await AsyncStorage.removeItem(STORAGE_KEY[key])
  } catch (error) {
    console.log(error);
  }
}

const clearStorage = async () => {
  try {
    await removeFromStorage('token')
    await removeFromStorage('stevedores')
    await removeFromStorage('mapLocation')
    await removeFromStorage('userLocation')
    await removeFromStorage('radius')
    await removeFromStorage('isSearch')
    await removeFromStorage('showed')
    return false
  } catch (error) {
    console.log(error);
  }
}

export { readFromStorage, writeToStorage, removeFromStorage }
