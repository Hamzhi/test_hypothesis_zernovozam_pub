import Toast from 'react-native-root-toast';
import {theme} from "../core/theme";


export const toastMessage = (message, type) => Toast.show(message, {
    duration: Toast.durations.SHORT,
    position: 50,
    shadow: true,
    animation: true,
    hideOnPress: true,
    textColor: type === 'error' ? theme.colors.tint : theme.colors.white,
    backgroundColor: type === 'error' ? theme.colors.error : theme.colors.success,
});

export default toastMessage
