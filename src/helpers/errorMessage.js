import {toastMessage} from "./toast";

export default function errorMessage(message, error){
    let mes = 'message';
    if (error && error.data)
        mes += (error.data.error || '') + (error.data.message || '');
    toastMessage(mes, 'error')
}
