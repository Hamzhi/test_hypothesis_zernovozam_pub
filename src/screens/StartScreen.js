import {View} from "react-native";
import {AuthContext} from "../services/AuthProvider";
import PhoneScreen from "./AuthScreens/PhoneScreen";
import LocationScreen from './AuthScreens/LocationScreen';
import MapScreen from "./MainScreens/MapScreen";
import React, {useContext, useEffect, useState} from "react";
import ZernoLoader from "../components/ZernoLoader";
import AuthWrapper from "../components/AuthWrapper";
import {readFromStorage} from "../services/StorageService";
import Logo from "../components/Logo";
import { LOCATION_STATUSES } from '../../constants';


export default function StartScreen(props) {
    const {
      userToken,
      login,
      radiusMap,
      setRadius,
      userLocation,
      setUserLocation,
      setMapLocation,
      setSearchingJob,
      setShowed,
      setStevedore
    } = useContext(AuthContext)
    const [loading, setLoading] = useState(true)
    const checkLocalStore = () => {
        Promise.all([
            readFromStorage('radius'),
            readFromStorage('mapLocation'),
            readFromStorage('userLocation'),
            readFromStorage('token'),
            readFromStorage('isSearch'),
            readFromStorage('showed'),
            readFromStorage('stevedores')
        ])
            .then(([radius, mapLocation, userLocation, token, isSearch, showed, stevedoreIds]) => {
                setRadius(radius || radiusMap, false)
                if (mapLocation) setMapLocation(mapLocation, false)
                if (userLocation) setUserLocation(userLocation, false)
                if (stevedoreIds) setStevedore(stevedoreIds, false)
                if (token) login(token, false)
                if (isSearch !== null) setSearchingJob(isSearch, false)
                if (showed !== null) setShowed(showed, false)
            })
            .finally(() => setLoading(false))
    }
    useEffect(checkLocalStore, [])

    if (loading) {
        return (
            <AuthWrapper>
                <View style={{flex:1, alignItems: 'center'}}>
                    <Logo />
                    <ZernoLoader />
                </View>

            </AuthWrapper>
        )
    }
    if (!userToken) return <PhoneScreen {...props} />

    return userLocation?.status !== LOCATION_STATUSES.untouched
      ? <MapScreen {...props} />
      : <LocationScreen {...props} />
}
