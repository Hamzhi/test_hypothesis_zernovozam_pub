import React, { useEffect, useState } from 'react';
import  {Animated, MarkerAnimated, PROVIDER_GOOGLE} from 'react-native-maps';
import {StyleSheet, View, Dimensions, Platform} from 'react-native';
import {AuthContext} from "../../services/AuthProvider";
import { IconButton, Switch, Title } from "react-native-paper";
import Button from "../../components/Button";
import TextInput from "../../components/TextInput";
import Background from "../../components/Background";
import MapCircle from "../../components/MapCircle";
import MapStyledMarker from "../../components/MapStyledMarker";
import MapFilterCard from "../../components/MapFilterCard";
import OnBoardingDialog from '../../components/OnBoardingDialog';
import LogService from '../../services/LogService';
import { logs } from '../../core/logs';
import { LOCATION_STATUSES } from '../../../constants';
import SwipeButton from '../../components/SwipeButton';

const initialStevedoreIds =[
    {
        "id": 1,
        "name": "НОВОРОССИЙСК"
    },
    {
        "id": 2,
        "name": "ЗТКТ ВОЛНА"
    },
    {
        "id": 3,
        "name": "РОСТОВ-НА-ДОНУ, АЗОВ"
    },
    {
        "id": 4,
        "name": "Другие"
    }
]

const onBoardings = {
    1: {
        log: logs.passFirstOnBoarding.action,
        text: 'Мы рады приветстовать Вас\n в нашем сервисе для\n владельцев зерновозов',
        uri: 'http://tapi2.zernovozam.ru/new-zernovozam/docs/swagger?token=docs_access#/'
    },
    2: {
        log: logs.passSecondOnBoarding.action,
        buttonText: 'ЗАКРЫТЬ ПОДСКАЗКУ',
        uri: 'http://tapi2.zernovozam.ru/new-zernovozam/docs/swagger?token=docs_access#/'
    },
    3: {
        log: logs.passThirdOnBoarding.action,
        text: 'Мы подберем подходящие заявки для вас.\n Список будет пополяться по мере нахождения новых заявок',
        uri: 'http://tapi2.zernovozam.ru/new-zernovozam/docs/swagger?token=docs_access#/'
    },
}

export default function MapScreen() {
    const [onBoarding, setOnBoarding] = React.useState(null);
    const [loading, setLoading] = useState(false);
    const [formStep, setFormStep] = useState(1);
    const {
        setRadius,
        userLocation,
        mapLocation,
        setMapLocation,
        radiusMap,
        setStevedore,
        stevedoreIds,
        searchingJob,
        setSearchingJob,
        showed,
        setShowed,
    } = React.useContext(AuthContext);

    useEffect(() => {
        if (showed < 1) {
            setOnBoarding(onBoardings[1])
        }
    }, [])

    const formConfirm = (step) => {
        if (showed < 3 && showed < step) {
            setOnBoarding(onBoardings[step])
        }

        setFormStep(step)
    }

    const location = userLocation?.status === LOCATION_STATUSES.granted ? userLocation : mapLocation
    const onSuccess = async () => {
        setLoading(true)
        const data = { action: onBoarding.log }
        if (userLocation?.status === LOCATION_STATUSES.granted) {
            data.user_lat = userLocation.latitude
            data.user_lon = userLocation.longitude
        }
        await LogService.sendLog(data)
          .then(() => {
              setOnBoarding(null)
              setShowed(formStep)
          })
          .finally(() => setLoading(false))
    }

    return (
      <Background>
          <SwipeButton status={searchingJob} disabled={formStep !== 3} setStatus={setSearchingJob} />
          {formStep === 1 && (
              <MapFilterCard text={'Выберите на карте центр территории для поиска, а ниже радиус для поиска и порты выгрузки '}>
                  <Title style={{margin: 0, padding: 0}}>Радиус:</Title>
                  <TextInput value={radiusMap} onChange={setRadius}/>
                  <Button mode={'contained'} onPress={() => formConfirm(2)}>ДАЛЕЕ</Button>
              </MapFilterCard>
          )}
          {formStep === 2 && (
            <MapFilterCard text={'Выберите  порты выгрузки '}>
              {initialStevedoreIds.map(stevedore => (
                <View  key={stevedore.id} style={{flex: 1, flexDirection: 'row', marginVertical: 8}}>
                    <Switch
                      value={stevedoreIds.includes(stevedore.id)}
                      onValueChange={() => setStevedore(stevedore.id)}
                    />
                    <Title style={{marginLeft: 12, textTransform: 'uppercase', fontSize: 18}}>
                        {stevedore.name}
                    </Title>
                </View>
              ))}
              <Button mode={'contained'} onPress={() => formConfirm(3)}>ДАЛЕЕ</Button>
            </MapFilterCard>)}
          {formStep === 3 && (
              <MapFilterCard
                showFilterPress={() => formConfirm(4)}
                showFilter={true}
                text={'Поиск заявок приостановлен, для поиска сдвиньте ползунок сверху'}
              />
          )}
          {formStep === 4 && (
            <MapFilterCard>
                <View style={styles.changeFilter}>
                    <Title style={styles.textFilter}>ИЗМЕНИТЬ ФИЛЬТР</Title>
                    <IconButton
                      icon="close"
                      style={styles.closeBtn}
                      size={23}
                      onPress={() => formConfirm(3)}
                    />
                </View>

                <Button mode={'contained'} onPress={() => formConfirm(1)}>ИЗМЕНИТЬ РАДИУС</Button>
                <Button mode={'contained'} onPress={() => formConfirm(2)}>ИЗМЕНИТЬ ВЫГРУЗКУ</Button>
            </MapFilterCard>
          )}
          <Animated
            loadingIndicatorColor={true}
            rotateEnabled={false}
            scrollEnabled={true}
            pitchEnabled={true}
            zoomTapEnabled={true}
            zoomEnabled={true}
            cacheEnabled={false}
            maxZoomLevel={Platform.OS === 'ios' ? 20 : Math.max(20, 10)}
            minZoomLevel={0}
            provider={PROVIDER_GOOGLE}
            zoomControlEnabled={true}
            showsUserLocation
            showsScale
            showsBuildings
            // showsMyLocationButton
            // followsUserLocation
            loadingEnabled
            showsTraffic={false}
            onPress={e => setMapLocation({...location, ...e.nativeEvent.coordinate})}
            region={location}
            style={styles.map}
          >
              <MarkerAnimated
                draggable
                onDragEnd={(e) => setMapLocation(e.nativeEvent.coordinate )}
                coordinate={location}
              >
                  <MapStyledMarker/>
              </MarkerAnimated>
              <MapCircle radius={radiusMap} location={location} />
          </Animated>
          <OnBoardingDialog data={onBoarding} loading={loading} onSuccess={onSuccess} />
      </Background>
    );
}

const styles = StyleSheet.create({
    map: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
    },
    changeFilter: {
        position: 'relative',
        height: 50,
    },
    textFilter: {
        margin: 0,
        padding: 0,
        textAlign: 'center',
        fontWeight: 'bold'
    },
    closeBtn: {
        position: 'absolute',
        margin: 0,
        right: 0,
        top: 0,
    }
});
