import React from 'react'
import { WebView } from 'react-native-webview'
import { View } from 'react-native';
import ZernoLoader from '../../components/ZernoLoader'

export default function CustomWebView({ uri }) {
  return (
    <WebView
      source={{ uri }}
      startInLoadingState
      javaScriptEnabled
      domStorageEnabled
      onHttpError={({ nativeEvent }) => {
        console.warn('onHttpError', nativeEvent.statusCode)
      }}
      renderLoading = {() => (
        <View style={{ flex:1, alignItems: 'center' }}>
          <ZernoLoader />
        </View>
      )}
    />
  )
}
