import React, { useState } from 'react';
import { Text, View, StyleSheet, TouchableOpacity, Image } from 'react-native';
import Background from '../../components/Background';
import Spinner from 'react-native-loading-spinner-overlay';
import Header from '../../components/Header';
import Paragraph from '../../components/Paragraph';
import Button from '../../components/Button';
import CloseButton from '../../components/CloseButton';
import { AuthContext } from '../../services/AuthProvider';
import * as Location from 'expo-location';
import {toastMessage} from "../../helpers/toast";
import LogService from '../../services/LogService';
import { logs } from '../../core/logs';
import { LOCATION_STATUSES } from '../../../constants';

export default function LocationScreen({ navigation }) {
  const { mapLocation, setUserLocation } = React.useContext(AuthContext);
  const [loading, setLoading] = useState(false);

  const handlePress = async () => {
    setLoading(true);
    // Open the custom settings if the app has one
    const permsRequest = await Location.requestForegroundPermissionsAsync();
    const { status } = permsRequest;
    if (status !== 'granted') {
      toastMessage('Доступ к геопозиции не предоставлен')
      setLoading(false)
      return;
    }
    const {
      coords: { latitude, longitude },
    } = await Location.getCurrentPositionAsync({});
    setUserLocation({ ...mapLocation, latitude, longitude, status: LOCATION_STATUSES.granted });
    await LogService.sendLog({
      action: logs.setGeoConfig.action,
      user_lat: latitude,
      user_lon: longitude
    })
    navigation.navigate('Map')
  };

  const handlePressOwn = async () => {
    setLoading(true);
    setUserLocation({ status: LOCATION_STATUSES.denied });
    await LogService.sendLog({ action: logs.setGeoConfig.action })
    navigation.navigate('Map')
  };

  return (
    <Background>
      {loading && (
        <Spinner
          visible
          textContent="Загрузка..."
          textStyle={styles.spinnerTextStyle}
        />
      )}
      <CloseButton action={handlePressOwn} />

      <View class={styles.imageWrap}>
        <Image
          style={styles.image}
          source={require('../../assets/location.png')}
        />
        <Header>Включить геолокацию</Header>
        <Paragraph style={{ textAlign: 'center' }}>
          Выберите геолокацию где мы будем искать
        </Paragraph>
        <Paragraph style={{ textAlign: 'center' }}>
          погрузки для вас.
        </Paragraph>
        <Button style={{marginVertical: 40}} size="big" mode="contained" onPress={handlePress}>
          Дать доступ к геолокации телефона
        </Button>
        <TouchableOpacity onPress={handlePressOwn}>
          <Text
            style={{
              color: '#41D5FB',
              margin: 0,
              padding: 0,
              fontWeight: '600',
              alignSelf: 'center'
            }}
          >
            Указать вручную
          </Text>
        </TouchableOpacity>
      </View>
    </Background>
  );
}
const styles = StyleSheet.create({
  imageWrap :{
    textAlign: 'center',
    flex: 1,
    justifyContent: "space-around",
    alignItems: 'center',
    height: '100%'
  },
  spinnerTextStyle: {
    color: '#FFF',
  },
  image: {
    alignSelf: 'center',
    width: 226,
    height: 226,
    marginBottom: 50
  },
  btnContainer: {
    width: '50%',
    alignSelf: 'center',
  },
  inputPhoneContainer: {
    height: 200,
    minHeight: 200,
    width: '100%',
    padding: 16,
    backgroundColor: 'rgba(255, 255, 255, 0.9)',
    borderRadius: 20,
  },
  containerAvoidingView: {
    flex: 1,
    alignItems: 'center',
    padding: 10,
  },
  containerInput: {
    borderColor: '#C8D3E8',
    flexDirection: 'row',
    paddingHorizontal: 12,
    borderRadius: 10,
    backgroundColor: 'white',
    alignItems: 'center',
    borderWidth: 1,
    // borderBottomWidth: 1.5
  },
  openDialogView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  phoneInputStyle: {
    marginLeft: 5,
    flex: 1,
    height: 50,
  },
  viewBottom: {
    flex: 1,
    justifyContent: 'flex-end',
    marginBottom: 50,
    alignItems: 'center',
  },
  btnContinue: {
    width: 150,
    height: 50,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textContinue: {
    color: '#fff',
    alignItems: 'center',
  },
});
