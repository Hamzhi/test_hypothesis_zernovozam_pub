import React, {useState} from 'react'
import Logo from "../../components/Logo";
import ImgBackground from "../../components/ImgBackground";
import {StyleSheet, View} from "react-native";
import PhoneInput from "../../components/PhoneInput";
import AuthService from "../../services/AuthService";
import {AuthContext} from "../../services/AuthProvider";
import Spinner from 'react-native-loading-spinner-overlay';
import Button from "../../components/Button";
import {toastMessage} from "../../helpers/toast";
import errorMessage from "../../helpers/errorMessage";
import LogService from '../../services/LogService';
import { logs } from '../../core/logs';

export default function PhoneScreen({ navigation }) {
    const { setPhone } = React.useContext(AuthContext)

    const [phoneNumber, setPhoneNumber] = useState('');
    const [isValidUser, setIsValidUser] = useState(true);
    const [loading, setLoading] = useState(false);
    const onChangePhone = (number) => setPhoneNumber(number)

    const loginHandle = () => {
        if (phoneNumber.length === 18) {
            setLoading(true)
            setIsValidUser( true)
            toastMessage('Вам было отправленно смс c кодом')
        } else {
            setIsValidUser( false)
            return
        }
        const formData = {  phone: phoneNumber.replace(/\D/g,"") }

        AuthService.authRequestSms(formData)
            .then(async ({ data }) => {
                setPhone(formData.phone)
                if (data?.sent) {
                    await LogService.sendLog({ action: logs.getSms.action })
                    navigation.navigate('Authentication')
                } else {
                    toastMessage('Сообщение не отправленно', 'error')
                }
            })
            .catch(err => errorMessage('Ошибка входа', err))
            .finally(() => setLoading(false))
    }

    return (
    <ImgBackground>
        <Logo />
        {loading && (<Spinner
            visible
            textContent={'Загрузка...'}
            textStyle={styles.spinnerTextStyle}
        />)}

        <View style={styles.inputPhoneContainer}>
            <PhoneInput
                isValid={isValidUser}
                onChange={onChangePhone}
                val={phoneNumber}
                setOpened={true}
            />
            <View style={styles.btnContainer}>
                <Button mode={"contained"} onPress={loginHandle}>Войти</Button>
            </View>
        </View>
    </ImgBackground>
  )
}

const styles = StyleSheet.create({
    spinnerTextStyle: {
        color: '#FFF'
    },
    btnContainer:{
        width: '50%',
        alignSelf: 'center'
    },
    inputPhoneContainer:{
        height: 200,
        minHeight: 200,
        width: '100%',
        padding: 16,
        backgroundColor: 'rgba(255, 255, 255, 0.9)',
        borderRadius: 20
    },
    containerAvoidingView: {
        flex: 1,
        alignItems: 'center',
        padding: 10
    },
    containerInput: {
        borderColor: "#C8D3E8",
        flexDirection: "row",
        paddingHorizontal: 12,
        borderRadius: 10,
        backgroundColor: 'white',
        alignItems: 'center',
        borderWidth: 1
        // borderBottomWidth: 1.5
    },
    openDialogView: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center"
    },
    phoneInputStyle: {
        marginLeft: 5,
        flex: 1,
        height: 50
    },
    viewBottom: {
        flex: 1,
        justifyContent: 'flex-end',
        marginBottom: 50,
        alignItems: 'center'
    },
    btnContinue: {
        width: 150,
        height: 50,
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    textContinue: {
        color: '#fff',
        alignItems:'center'
    }

})

