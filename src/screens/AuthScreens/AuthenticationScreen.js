import React, { useState } from 'react';
import { TouchableOpacity, StyleSheet, View } from 'react-native';
import { TextMask } from 'react-native-masked-text'
import { Text} from 'react-native-paper';
import Background from '../../components/Background';
import Header from '../../components/Header';
import Button from '../../components/Button';
import BackButton from '../../components/BackButton';
import { theme } from '../../core/theme';
import Paragraph from '../../components/Paragraph';
import OtpSmsCode from '../../components/OtpSmsCode';
import Toast from 'react-native-root-toast';
import AuthService from '../../services/AuthService';
import { AuthContext } from '../../services/AuthProvider';
import Spinner from 'react-native-loading-spinner-overlay';
import {toastMessage} from "../../helpers/toast";
import errorMessage from "../../helpers/errorMessage";
import LogService from '../../services/LogService';
import { logs } from '../../core/logs';

export default function AuthenticationScreen({ navigation }) {
  const { phone, login } = React.useContext(AuthContext);
  const [smsCode, setSmsCode] = useState('');
  const [invalidCode, setInvalidCode] = useState(false);
  const [loading, setLoading] = useState(false);

  const requestNewCode = () => {
    const formData = { phone };
    setLoading(true);

    AuthService.authRequestSms(formData)
      .then(() => {
        toastMessage('Вам было отправленно повторно смс')
      })
      .catch((err) => {
        errorMessage('Ошибка, смс не отправленно', err)
      })
      .finally(() => setLoading(false));
  };

  const loginHandle = () => {
    if (smsCode.length === 4) {
      setInvalidCode(false);
    } else {
      Toast.show('Код должен состоять из 4 цифр', {
        duration: Toast.durations.LONG,
        position: 99,
        shadow: false,
        animation: true,
        hideOnPress: true,
        textColor: '#222',
        backgroundColor: 'rgba(65, 213, 251, 0.9)',
      });
      return;
    }
    const formData = { phone, code: smsCode };
    setLoading(true);
    AuthService.authGetToken(formData)
      .then(async ({ data }) => {
        if (data?.token) {
          await LogService.sendLog({ action: logs.sendSms.action })
          login(data.token)
          navigation.navigate('Location')
        }
      })
      .catch((err) => {
        errorMessage('Ошибка, смс не отправленно', err)
        setInvalidCode(true);
      })
      .finally(() => setLoading(false));
  };

  return (
    <Background>
      {loading && (
        <Spinner
          visible
          textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
        />
      )}

      <BackButton goBack={navigation.goBack} />
      <View>
        <Header>Проверка номера телефона</Header>
        <Paragraph>
          Проверьте ваши SMS сообщения, мы выслали вам код подтверждения
          телефона на
        </Paragraph>
        <TextMask
          type="custom"
          options={{ mask: '+7 (999) 999 99 99' }}
          value={phone}
          style={styles.phoneLink}
        />
      </View>
      <View style={{ width: '100%' }}>
        <OtpSmsCode
          setSmsCode={setSmsCode}
          invalidCode={invalidCode}
          setInvalidCode={setInvalidCode}
        />
        <Button size="big" mode="contained" onPress={loginHandle}>
          Войти
        </Button>
      </View>
      <View style={styles.textLinkContainer}>
        <Text>Не приходит SMS?</Text>
        <TouchableOpacity onPress={requestNewCode}>
          <Text
            style={{
              color: '#41D5FB',
              margin: 0,
              padding: 0,
              fontWeight: '600',
            }}
          >
            {' '}
            Выслать новый код
          </Text>
        </TouchableOpacity>
      </View>
    </Background>
  );
}

const styles = StyleSheet.create({
  textLinkContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },

  phoneLink: {
    color: theme.colors.primary,
    fontSize: 15,
    fontWeight: 'bold',
  },
  row: {
    flexDirection: 'row',
    marginTop: 4,
  },

  link: {
    fontWeight: 'bold',
    color: theme.colors.primary,
  },
  forgotPassword: {
    width: '100%',
    alignItems: 'flex-end',
    marginBottom: 24,
  },
  forgot: {
    fontSize: 13,
    color: theme.colors.secondary,
  },
});
